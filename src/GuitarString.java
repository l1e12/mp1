import java.util.LinkedList;
import java.util.Queue;


// The Guitar String Class
public class GuitarString {
	
	public Queue<Double> ringBuffer;
	private double frequency;
	private static final double ENERGY_DECAY_CONSTANT = 0.996;
	private static final int SAMPLE_RATE = 44100;
	

	/**
	 * Creates a guitar string with a provided frequency.
	 * 
	 * @Param The frequency of the string to be played.
	 * @Throws IllegalArgumentException if the size of the ring buffer is less than 2 
	 * or the frequency input is less than zero.
	 */
	public GuitarString(double frequency) throws IllegalArgumentException{
		this.frequency = frequency;	
		
		//Get the size which will define the ringBuffer queue with N doubles
		int N = (int)Math.round(SAMPLE_RATE/frequency);
		ringBuffer = new LinkedList<Double>();
		
		//Initialization of a string at rest.
		for( int index = 0; index < N ; index++){
			ringBuffer.add(0.0);
		}
		
		//Throws an exception if the value of frequency is too low or too high.
		if(frequency < 0 || N < 2){
			throw new IllegalArgumentException("Your input frequency is out of bounds");
		}
		
	}
	
    /**
     * Creates a guitar string with a provided array of ring buffer values.
     * 
     * @Param The array of ring buffer values.
     * @Throws IllegalArgumentException if the array of ring buffers is smaller than two
     */
	public GuitarString(double[] init) throws IllegalArgumentException{
		ringBuffer = new LinkedList<Double>(); 
		
		//Get the size of the array
	    int size = init.length;
	    
	    //Adds the doubles form the array and defines the of the ringBuffer 
	    for( int index = 0; index < size; index++){
	    	ringBuffer.add(init[index]);
	    }
	    
	    //Throws an exception if the ring buffer is < 2
		if(ringBuffer.size() < 2){
			throw new IllegalArgumentException("Your ring buffer needs more than one input");
		}
	}
	/**
	 * Effect: Replaces all of the elements in the ring buffer with random values between
	 *         -0.5 and +0.5.
	 */
	public void pluck(){
		//Loop to replace all of the ringBuffer values with random ones.
		for(int index = 0; index < ringBuffer.size(); index ++){
			double randomVal = Math.random() * (-0.5) + 0.5;
			ringBuffer.remove();
			ringBuffer.add(randomVal);	
		}
	}
	
	/**
	 * Applies the Karplus-Strong algorithm once to the ringBuffer
	 * which simulates a low pass filter.
	 */
	public void tic(){
		double firstVal = ringBuffer.remove();
		double secondVal = ringBuffer.peek();
		
		//Apply the formula and add to the rear of the ringBuffer
		ringBuffer.add(ENERGY_DECAY_CONSTANT * 0.5 * (firstVal + secondVal));
	}

    /**
     * @return the value of the the head of the ring buffer queue
     */
    public double sample(){
    	return ringBuffer.peek();
    }
}
	
