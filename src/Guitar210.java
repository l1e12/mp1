import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

// skeleton version of the class

public class Guitar210 implements Guitar {
    public static final String KEYBOARD =
    		"q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ";  // keyboard layout
    private HashMap<Character, GuitarString> keyToFreq;
    private static int numTic;
    
    /**
     * Initializes a guitar with keys on the keyboard mapped to GuitarStrings with given frequencies
     */
    public Guitar210(){
    	
    	//Initialize the map between each key and each string
    	keyToFreq = new HashMap<Character, GuitarString>();
    	
    	//Creates a map between the keyboard char and its 37 strings
    	for(int index = 0; index < KEYBOARD.length(); index++){
   
    		double frequency = 440.0 * Math.pow(2, (index - 24.0) / 12.0);
    		GuitarString newString = new GuitarString(frequency);
    		keyToFreq.put(KEYBOARD.charAt(index), newString);
    	}
    	
    	//Tic has not been called yet
    	numTic = 0;
    	
    }
    
    /**
     * Takes a pitch, generates an index (pitch + 12), then plays a note of the given pitch
     * 
     * @param integer pitch value
     */
	@Override
	public void playNote(int pitch) {
		int index = pitch + 12;
		keyToFreq.get(KEYBOARD.charAt(index)).pluck();
	}
	
	/**
	 * Checks the map to see if the entered character has a corresponding string.
	 * 
	 * @param: Key - The character to check for
	 * @return: True if a string for this character exists, else false.
	 */
	public boolean hasString(char key) {
		if(keyToFreq.containsKey(key))
			return true;
		else
			return false;
	}
	
    /**
     * Plucks the desired string to replace all of its ring buffer values with doubles 
     * between -0.5 and 0.5
     * 
     * @param Key - The character who's corresponding string is to be plucked
     * @throws IllegalArgumentException if the key pressed is not part of KEYBOARD
     */
	public void pluck(char key) throws IllegalArgumentException {
		if(KEYBOARD.indexOf(key) == -1)
			throw new IllegalArgumentException("Invalid key");
		GuitarString pluckedString = keyToFreq.get(key);
		pluckedString.pluck();
	}

	/**
	 * Calculates the sum of ring buffers currently being played
	 * 
	 * @return The sum of all of the ring buffers being played
	 */
	public double sample() {
		double sum = 0.0;
		for(GuitarString stringSample : keyToFreq.values()){
			sum += stringSample.sample();
		}
		return sum;
	}

	/**
	 * Simulates a low pass filter on all strings on the guitar
	 */
	@Override
	public void tic() {
		numTic++;
		
		for(GuitarString ticString : keyToFreq.values()){
			ticString.tic();
		}
	}

	/**
	 * @return number of times tic() has been called
	 */
	@Override
	public int time() {
		return numTic;
	}

}